import React, { Component } from 'react';
import './App.css';
import Site from './Site';
import NotebookGamer from './NotebookGamer';
import BarraTransição from './BarraTransição';
import NotebookGamer2 from './notebookGamer2';
import NotebookGamer3 from './NotebookGamer3';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Site />
        <NotebookGamer />
        <BarraTransição />
        <NotebookGamer2 />
        <BarraTransição />
        <NotebookGamer3 />
        <BarraTransição />
      </div>
    );
  }
}

export default App;
