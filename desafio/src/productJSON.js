export default {
    "productId": 1167,
    "name": "5 Camisas Elite Polo Live Masculina - K5-1016-0103040506",
    "salesChannel": "1",
    "available": true,
    "dimensions": [
        "tamanho"
    ],
    "dimensionsMap": {
        "tamanho": [
            "P",
            "M",
            "G",
            "GG"
        ]
    },
    "skus": [
        {
            "sku": 4929,
            "skuname": "Eu sou o Gallo",
            "dimensions": {
                "tamanho": "P"
            },
            "available": false,
            "availablequantity": 0,
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "R$ 239,90",
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 0,00",
            "bestPrice": 0,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "http://vestaria.vteximg.com.br/arquivos/ids/162825-650-910/kit5-elite.jpg?v=636882680834600000",
            "sellerId": "1",
            "seller": "Vestaria Shop",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4930,
            "skuname": "5 Camisas Elite Polo Live Masculina - K5-1016-0103040506 - M",
            "dimensions": {
                "tamanho": "M"
            },
            "available": false,
            "availablequantity": 0,
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "R$ 239,90",
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 0,00",
            "bestPrice": 0,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "http://vestaria.vteximg.com.br/arquivos/ids/162826-650-910/kit5-elite.jpg?v=636882685227900000",
            "sellerId": "1",
            "seller": "Vestaria Shop",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4931,
            "skuname": "5 Camisas Elite Polo Live Masculina - K5-1016-0103040506 - G",
            "dimensions": {
                "tamanho": "G"
            },
            "available": true,
            "availablequantity": 99999,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "R$ 239,90",
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 239,90",
            "bestPrice": 23990,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "http://vestaria.vteximg.com.br/arquivos/ids/162827-650-910/kit5-elite.jpg?v=636882687184700000",
            "sellerId": "1",
            "seller": "Vestaria Shop",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4932,
            "skuname": "5 Camisas Elite Polo Live Masculina - K5-1016-0103040506 - GG",
            "dimensions": {
                "tamanho": "GG"
            },
            "available": true,
            "availablequantity": 5,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "R$ 200,90",
            "listPrice": 200.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 239,90",
            "bestPrice": 23990,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "http://vestaria.vteximg.com.br/arquivos/ids/162828-650-910/kit5-elite.jpg?v=636882691869600000",
            "sellerId": "1",
            "seller": "Vestaria Shop",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        }
    ]
}