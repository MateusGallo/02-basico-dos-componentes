import React, { Component,Fragment } from 'react'

class NotebookGamer2 extends Component{
    constructor(props){
        super(props)
        this.alerta=this.alerta.bind(this)
    }
    alerta(event){
        event.preventDefault()
        alert("Você adicionou ao carrinho o produto: Avell A62-7 por R$5.799,30 ")
    }
    
    render() {
        return (
    
    <div className= "Notebook1">
    
        <div className= "imagem">
            <img src="https://images.avell.com.br/media/catalog/product/cache/1/thumbnail/800x600/9df78eab33525d08d6e5fb8d27136e95/a/v/avell-1_5_5.jpg"></img>
                <div className= "informações">
        <center><div className='nomeProd'>Avell A62-7</div></center>
            <div className='cor'>Workstation Series  Notebook para uso Profissional
                <del><div className= 'preço'>de R$6.999,30</div></del>
                     <div className= 'preço1'>por R$5.799,30</div>
                     <button className="botao" onClick={this.alerta}>COMPRAR</button>
                </div>
            </div>
        </div>
    </div>
        )
    }
}

    
    export default NotebookGamer2