import React, { Component,Fragment } from 'react'

class NotebookGamer3 extends Component{
    
    constructor(props){
        super(props)
        this.alerta=this.alerta.bind(this)
    }
    alerta(event){
        event.preventDefault()
        alert("Você adicionou ao carrinho o produto: Avell G1711 FOX i7+ por R$4.999,30 ")
    }
    
    render() {

     return (
    <div className= "Notebook2">
    
        <div className= "imagem2">
            <img src="https://images.avell.com.br/media/catalog/product/cache/1/thumbnail/800x600/9df78eab33525d08d6e5fb8d27136e95/g/1/g1744fox_7_.jpg"></img>
                <div className= "informações">
        <center><div className='nomeProd'>Avell G1711 FOX i7+</div></center>
            <div className='cor'>Workstation Series  Notebook Para uso Geral
                <del><div className= 'preço'>de R$5.999,30</div></del>
                     <div className= 'preço1'>por R$4.999,30</div>
                     <button className="botao" onClick={this.alerta}>COMPRAR</button>
                </div>
            </div>
        </div>
    </div>
        )
    }
}
      
    
    export default NotebookGamer3