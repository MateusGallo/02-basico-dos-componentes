import React, { Component,Fragment } from 'react';

class NotebookGamer extends React.Component {
    
    constructor(props){
        super(props)
        this.alerta=this.alerta.bind(this)
    }
    alerta(event){
        event.preventDefault()
        alert("Você adicionou ao carrinho o produto: Avell A65 RTX por R$7.799,30 ")
    }

    render() {
        return (
<div className= "Notebook1">

    <div className= "imagem">
        <img src="https://images.avell.com.br/media/catalog/product/cache/1/thumbnail/800x600/9df78eab33525d08d6e5fb8d27136e95/g/1/g1550-144hz-_5__1_8.jpg"></img>
            <div className= "informações">
    <center><div className='nomeProd'>Avell A65 RTX</div></center>
        <div className='cor'>Workstation Series  Notebook Gamer
            <del><div className= 'preço'>de R$8.799,30</div></del>
                 <div className= 'preço1'>por R$7.799,30</div>
                 <button className="botao" onClick={this.alerta}>COMPRAR</button> 
            </div>
        </div>
    </div>
</div>
    )
 }

}

export default NotebookGamer